import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';
import { DataService } from '../shared/providers/data.service';

/**
*	This class rep resents the lazy loaded LoginComponent.
*/
const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;


@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  public myForm: FormGroup;
  post: any;
  email: String = '';
  password: String = '';
  name: String = '';
  accessTokenId: string;
  myData: any;

  
  
    constructor(
              private router: Router,
              private dataService: DataService,
              private toasterService: ToasterService
    ) { }
    ngOnInit() {
        this.myForm = new FormGroup({
            email: new FormControl('', Validators.compose([Validators.required,
                                                                   Validators.pattern(EMAIL_REGEX)
                                                                  ])),
            name: new FormControl('', Validators.required),
                                                     
            password: new FormControl('', Validators.required)
      });
    }

    signup(post): void {
        let self = this;
        console.log(post);
     
      
        this.dataService.signup(post)
             .subscribe(
                  res => {
                          this.myData = res;
                          console.log(this.myData);
                          if (this.myData.success) {
                                 this.toasterService.pop('success', 'success', this.myData.msg);
                                 self.signin({email: post.email, password:post.password});
                      
                
                          } else {
                            this.toasterService.pop('error', 'Error', this.myData.msg);
                          }
                       
                         },
                  error => {
                   this.toasterService.pop('error', 'Error', 'some server error');
                   self.router.navigate(['/']);
                  

                    }
                       ); 

   }

   signin(post): void {
    let self = this;
    console.log(post);
 
  
    this.dataService.login(post)
         .subscribe(
              res => {

                      this.myData = res;
                      console.log(this.myData);

                      if (this.myData.success) {
                             localStorage.setItem('auth', this.myData.token);
                 
                             let url = '/userinfo';
                             self.dataService.get(url)
                                        .subscribe(
                                            res => {
                                                    self.myData = res;
                                                    console.log(res);
                                                    if (self.myData.success) {
                                                         localStorage.setItem('username', self.myData.username);
                                                         self.toasterService.pop('success', 'success', self.myData.msg);
                                            
                                                          self.router.navigate(['./dashboard/home']);
                                                        } else {
                                                         self.toasterService.pop('error', 'error', self.myData.msg);
                                                         self.router.navigate(['/login']);
                                                        }
                                                    },
                                             error =>  {
                                                       self.toasterService.pop('error', 'error', 'Some error on server');
                                                       self.router.navigate(['/login']);
                                                  }
                                          );
            
                     
                     
                     
                     
                     
                     
                           } else {
                               this.toasterService.pop('error', 'Error', this.myData.msg);
                               self.router.navigate(['./login']);
                            }    



                    
                     
                     },
              error => {
               this.toasterService.pop('error', 'Error', 'Some error on server');
                    

                }
                   ); 

}


}
