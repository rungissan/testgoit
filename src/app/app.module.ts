import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule, Http, BrowserXhr } from '@angular/http';
import { NgProgressModule, NgProgressBrowserXhr } from 'ngx-progressbar';
import { ToasterModule } from 'angular2-toaster';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { DataService } from './shared/index';
import { DialogService } from 'ng2-bootstrap-modal';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { DialogsModule } from './shared/components/dialogs.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginModule } from './login/login.module';
import { SignupModule } from './signup/signup.module';

@NgModule({
  declarations: [
    AppComponent
   
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    AppRoutingModule,
    ToasterModule,
    LoginModule,
    SignupModule,
    NgbModule.forRoot(),
    NgProgressModule,
    DialogsModule
    
  ],
  providers: [
              { provide: BrowserXhr, useClass: NgProgressBrowserXhr },
               DataService,
               DialogService
            ],
   bootstrap: [AppComponent]
})
export class AppModule { }
