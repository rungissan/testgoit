import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { DataService } from './shared/providers';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'My mappoints application';
  public myData: any;
  constructor(
    private toasterService: ToasterService,
    public dataService: DataService,
    private router: Router
  ) { };
  ngOnInit() {
    let self = this ;

     let accessTokenId = localStorage.getItem('auth');
     console.log(accessTokenId);
     if ( accessTokenId!==null ) {
                             console.log('this.accessTokenId : ' + accessTokenId);
                             let url = '/userinfo';
                             this.dataService.get(url)
                                        .subscribe(
                                            res => {
                                              this.myData = res;
                                              console.log(this.myData);
                                              if (this.myData.success) {
                                                     this.toasterService.pop('success', 'success', this.myData.msg);
                                                     self.router.navigate(['./dashboard/home']);
                                                    
                                          
                                    
                                              } else {
                                                this.toasterService.pop('error', 'Error', this.myData.msg);
                                                self.router.navigate(['./login']);
                                              }      

                                                   
                                                    },
                                             error =>  {
                                      self.toasterService.pop('error','Error','Some server error');
                                      self.router.navigate(['./login']);
                                                  }
                                          );
                              }
    
        if (this.router.url === '/') {
            this.router.navigate(['login']);
        } 

  };
}

