import { Component , OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';
import { DataService } from '../shared/providers/data.service';

/**
*	This class rep resents the lazy loaded LoginComponent.
*/
const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
public myForm: FormGroup;
  post: any;
  email: String = '';
  password: String = '';
  accessTokenId: string;
  myData: any;

  
  public Name: String;


 constructor( private router: Router,
              private dataService: DataService,
              private toasterService: ToasterService
            ) { };
   ngOnInit() {
    
       this.accessTokenId = localStorage.getItem('auth');
       if ((this.accessTokenId !== null )) {
                                console.log('this.accessTokenId : ' + this.accessTokenId);
                                this.router.navigate(['/dashboard/home']);
                                }
     this.myForm = new FormGroup({
             email: new FormControl('', Validators.compose([Validators.required,
                                                                  Validators.pattern(EMAIL_REGEX)
                                                                 ])),
              password: new FormControl('', Validators.required)
     });
   }
   signin(post): void {
             let self = this;
             console.log(post);
          
           
             this.dataService.login(post)
                  .subscribe(
                       res => {

                               this.myData = res;
                               console.log(this.myData);

                               if (this.myData.success) {
                                      localStorage.setItem('auth', this.myData.token);
                          
                                      let url = '/userinfo';
                                      self.dataService.get(url)
                                                 .subscribe(
                                                     res => {
                                                             self.myData = res;
                                                             console.log('what is here')
                                                             console.log(res);
                                                             if (self.myData.success) {
                                                                  localStorage.setItem('username', self.myData.name);
                                                                  self.toasterService.pop('success', 'success', self.myData.msg);
                                                     
                                                                   self.router.navigate(['./dashboard/home']);
                                                                 } else {
                                                                  self.toasterService.pop('error', 'error', self.myData.msg);
                                                                  self.router.navigate(['/login']);
                                                                 }
                                                             },
                                                      error =>  {
                                                                self.toasterService.pop('error', 'error', 'Some error on server');
                                                                self.router.navigate(['/login']);
                                                           }
                                                   );
                     
                              
                              
                              
                              
                              
                              
                                    } else {
                                        this.toasterService.pop('error', 'Error', this.myData.msg);
                                        self.router.navigate(['./login']);
                                     }    



                             
                              
                              },
                       error => {
                        this.toasterService.pop('error', 'Error', 'Some error on server');
                             

                         }
                            ); 
 
        }
}