import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { TopnavComponent } from '../shared/';
import { SidebarComponent} from '../shared/components/sidebar/sidebar.component';


@NgModule({
    imports: [
        CommonModule,
        DashboardRoutingModule
    ],
    declarations: [
        DashboardComponent,
        TopnavComponent,
        SidebarComponent
      
    ]
})
export class DashboardModule { }
