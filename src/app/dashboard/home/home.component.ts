import { Component, AfterViewInit, ViewChild ,ElementRef} from '@angular/core';
import { DataService , GoogleMaps} from '../../shared/providers';
import { ToasterService } from 'angular2-toaster';


@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements AfterViewInit {
  
    @ViewChild('mapgoogle') mapElement: ElementRef;
   
    constructor(public dataService: DataService,
                private toasterService: ToasterService, 
                public maps: GoogleMaps
                ) {

    }

   
    ngAfterViewInit() {
        this.maps.initMap(this.mapElement.nativeElement);
      }
    setZoomPlus() {
        this.maps.setZoomPlus();
    }
    setZoomMinus() {
        this.maps.setZoomMinus();
    }
    saveMarkers() {
      console.log(this.maps.newmarkers);
      this.dataService.saveMarkers(this.maps.newmarkers)
            .subscribe(
                 res => {
                    let myData:any;
                    myData = res;
                    if (myData.success) {
                         this.toasterService.pop('success', 'success', myData.msg);
                         this.maps.newmarkers=[];
           
                        } else {
                        this.toasterService.pop('error', 'error', myData.msg);
                        }  
              
               },
         error =>  {
           this.toasterService.pop('error', 'bad','some server error');
         }
        );
    }

    searchRadar(value:string) {
        if (value !=='notchosen') {
               this.maps.getRadarRequest(value);
        }
  
    }

}
