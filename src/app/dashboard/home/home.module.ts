import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { GoogleMaps} from '../../shared/providers';


@NgModule({
    imports: [
        CommonModule,
        HomeRoutingModule
       
     ],
    declarations: [HomeComponent],
    providers: [GoogleMaps]
})
export class HomeModule { }
