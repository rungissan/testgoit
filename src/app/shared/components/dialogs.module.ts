import { NgModule } from '@angular/core';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MapDialogModule } from './dialog-map/map-dialog.module';



@NgModule({
    imports: [
        BootstrapModalModule,
        FormsModule,
        MapDialogModule,
        ReactiveFormsModule
       
           ],
    exports: [ ],
    declarations: [],
    providers: [],
    entryComponents: [ ],
})
export class DialogsModule { }

