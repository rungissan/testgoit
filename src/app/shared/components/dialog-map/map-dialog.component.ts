import { Component,OnInit,ViewChild, ElementRef} from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { FormControl, FormGroup, Validators} from '@angular/forms';



export interface ConfirmModel {
  markertitle: string;
  
}
@Component({
    selector: 'app-map-dialog',
    templateUrl: './map-dialog.component.html',
    styleUrls: ['./map-dialog.component.scss']
})
export class MapDialogComponent extends DialogComponent<ConfirmModel, ConfirmModel>
         implements ConfirmModel, OnInit {
   markertitle: string;
   @ViewChild('name') name: ElementRef;
   public myForma: FormGroup;
 
   constructor(public dialogService: DialogService ) {
    super(dialogService);
    console.log('initialization1');
    this.myForma = new FormGroup({
      name: new FormControl('vasya', [Validators.required])
     });
   
  }
  ngOnInit() {
       
               console.log('initialization');
        
   }
  

  confirm() {
   
    console.log(this.name.nativeElement.value);
    this.markertitle = this.name.nativeElement.value;
    
    this.result = { markertitle : this.markertitle};
    this.close();
  }

 
}
