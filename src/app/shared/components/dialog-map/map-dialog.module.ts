import { NgModule } from '@angular/core';
import { MapDialogComponent } from './map-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@NgModule({
    imports: [ FormsModule,
               ReactiveFormsModule,
               CommonModule
             
    ],
    exports: [
        MapDialogComponent
    ],
    declarations: [
        MapDialogComponent
    ],
      entryComponents: [
        MapDialogComponent
    ]
})
export class MapDialogModule { }
