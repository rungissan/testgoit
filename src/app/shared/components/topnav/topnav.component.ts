import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { Router } from '@angular/router';
import { DataService } from '../../providers/data.service';
import { ToasterService } from 'angular2-toaster/angular2-toaster';


declare var $: any;

@Component({
    selector: 'app-topnav',
    templateUrl: './topnav.component.html',
    styleUrls: ['./topnav.component.scss']
})
export class TopnavComponent implements OnInit {
    
    public username: string;
    public userId: string;
  
    constructor(
                private toasterService: ToasterService,
                private dataService: DataService,
                private router: Router) { 

          
                }

    ngOnInit() {
    
    }

   

  
      logout(): void {
       
       
         this.toasterService.pop('success', 'Досвидания', 'Вы успешно вышли !');
         localStorage.clear();
         this.router.navigate(['/login']);
     

              }
    getName(): string {
        return  localStorage.getItem('username');
    }
    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('push-right');
    }
   

}
