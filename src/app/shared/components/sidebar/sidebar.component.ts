import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';


import { ToasterService } from 'angular2-toaster/angular2-toaster';


@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    
    showMenu: String = '';
    user: any;



    constructor(private router:  Router,
               private toasterService: ToasterService) {
  
              }

    ngOnInit() { 
}

logout(): void {
       
       
    this.toasterService.pop('success', 'Bye', 'Good Bye, my friend !');
    localStorage.clear();
    this.router.navigate(['/login']);


         }
 
}
