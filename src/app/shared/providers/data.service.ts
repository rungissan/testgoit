import { Injectable, EventEmitter, Output } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
//import 'rxjs/add/observable/map';
// import 'rxjs/add/operator/do';  // for debugging

/**
* This class provides the Data service with methods to read names and add names.
*/
@Injectable()
export class DataService {
@Output() profileChange: EventEmitter<any> = new EventEmitter();


public apiUrl = 'http://old.fotosmart.pro:3012/api';

    /**
    * Creates a new DataService with the injected Http.
    * @param {Http} http - The injected Http.
    * @constructor
    */
    constructor(private http: Http) {}

    /**
    * Returns an Observable for the HTTP GET request for the JSON resource.
    * @return {string[]} The Observable for the HTTP request.
    */
   profileChangeEmit() {
        this.profileChange.emit();
        }
  
  get(url: string): Observable<any[]> {
        let auth = localStorage.getItem('auth');
        let headers  = new Headers({ 'Content-Type': 'application/json' });
        headers.append('Authorization', `${auth}`);
        let options       = new RequestOptions({ headers: headers });
        return this.http.get(this.apiUrl + url, options)
               .map((res: Response) => res.json())
                  //     .do(data => console.log('server data:', data))  // debug
               .catch(this.handleError);
    }
    

      /**
    * Handle HTTP error
    */
    private handleError (error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

    login(body: Object): Observable<any[]> {
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options       = new RequestOptions({ headers: headers }); // Create a request option

        return this.http.post(this.apiUrl + '/login', body, options) // ...using post request
                         .map((res: Response) => res.json()) // ...and calling .json() on the response to return data
                         .catch((error: any) =>  Observable.throw(error.json().error || 'Server error')
                                               ); //...errors if any

    }
    signup(body: Object): Observable<any[]> {
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options       = new RequestOptions({ headers: headers }); // Create a request option

        return this.http.post(this.apiUrl + '/signup', body, options) // ...using post request
                         .map((res: Response) => res.json()) // ...and calling .json() on the response to return data
                         .catch((error: any) =>  Observable.throw(error.json().error || 'Server error')
                                               ); //...errors if any

    }

  
 
  getPointofInterest(data: any): Observable< String[]> {
         let url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json';
        let headers  = new Headers({ 'Content-Type': 'application/json' });
        headers.append('Authorization', 'AIzaSyAPFv_lQLe_ie-m-KKixn5ww_07ZQBuVZg');
        headers.append('Access-Control-Allow-Origin', '*');
        let params = new URLSearchParams();
         for (let key in data) {
                params.set(key, data[key]);
              }
        let options       = new RequestOptions({ headers: headers, params: params });
        return this.http.get(url, options) // ...using post request
                         .map((res: Response) => res.json()) // ...and calling .json() on the response to return data
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }

    saveMarkers(newMarkers: any[]): Observable<any[]> {
        let auth = localStorage.getItem('auth');
        let headers  = new Headers({ 'Content-Type': 'application/json' });
        headers.append('Authorization', `${auth}`);
      
        return this.http.post(' http://old.fotosmart.pro:3012/api/newmarkers', JSON.stringify(newMarkers), {headers: headers})
            .map(res => res.json())
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
               
           
           
     
    }
 

}
