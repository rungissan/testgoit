/// <reference path="../../../../node_modules/@types/googlemaps/index.d.ts" />
import { Injectable, NgZone } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {  DialogService } from 'ng2-bootstrap-modal';
import { MapDialogComponent } from '../components/dialog-map/map-dialog.component';
declare var MarkerClusterer: any;
 
@Injectable()
export class GoogleMaps {
 
    map: any;
    markers: any = [];
    newmarkers: any = [];
    zoom = 18;
    markerCluster: any;

    constructor(public http: Http,
                private zone: NgZone,
                public dialogService:DialogService) {
 
    }
 
    initMap(mapElement){
        let marker;
        let lat = 46.452962;
        let lng = 30.760792;
        let title = 'здесь я!!!';
 
        let latLng = new google.maps.LatLng(lat,lng);
        console.log(mapElement);

        var styledMapType = new google.maps.StyledMapType(
            [
              {
                   featureType: 'poi',
                   stylers: [{ visibility: 'off' }]  // Turn off POI.
                 }
            ],
            {name: 'Styled Map'});
     
        let mapOptions = {
            center: latLng,
            zoom: this.zoom,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true,
            mapTypeControlOptions: {
                mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain',
                        'styled_map']
              }
        };
     
        this.map = new google.maps.Map(mapElement, mapOptions);
         //Associate the styled map with the MapTypeId and set it to display.
        this.map.mapTypes.set('styled_map', styledMapType);
        this.map.setMapTypeId('styled_map');
     
        google.maps.event.addListenerOnce(this.map, 'idle', () => {
      
     
        this.loadMarkers();
// my marker
         if(!this.markerExists(lat, lng)){

            let image = {
                url: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                // This marker is 20 pixels wide by 32 pixels high.
                size: new google.maps.Size(40, 64),
                // The origin for this image is (0, 0).
                origin: new google.maps.Point(0, 0),
                // The anchor for this image is the base of the flagpole at (0, 32).
                anchor: new google.maps.Point(0, 32)
              };
     
            marker = new google.maps.Marker({
                title: title,
                map: this.map,
                animation: google.maps.Animation.DROP,
                position: latLng,
                icon: image
            });
 
       /*     let markerData = {
                lat: lat,
                lng: lng,
                marker: marker
            }; */
            let infowindow = new google.maps.InfoWindow({
                content: title
              });
            marker.addListener('click', function() {
                infowindow.open(this.map, marker);
              });
 
          // this.testmarkers.push(markerData);
           this.markers.push(marker);

 
        }
  

        google.maps.event.addListener(this.map, 'dragend', () => {
            this.loadMarkers();
           });
       google.maps.event.addListener(this.map, 'click', (event) => {
               console.log('Фиксирую клик');
               console.log(event.latLng);
               this.placeMarker(event.latLng);
               
              
              });
        

     });
    
    
   


       
     
    }

 
  placeMarker(position) {
    let self =this;
    const ConfirmModel = {
        markertitle: null
     }

    let disposable = this.dialogService.addDialog(MapDialogComponent, ConfirmModel )
     .subscribe((addform) => {
        self.zone.run(() => {  
        console.log(addform);
        let title: string;
        console.log('я тут позицию нашел');
     
        console.log(position);

       
        if ( addform !== undefined ) {
            
            if (addform.markertitle !='') {
                title = addform.markertitle;
          } else {
              title = ' loc: ' + position.lat()+','+position.lng();
   
          }
       
     

       
          let  marker = new google.maps.Marker({
            title: title,
            map: self.map,
            animation: google.maps.Animation.DROP,
            position: position
            
        });
        console.log(position);
       
        let newMarkerData = {
            name: title,
            loc: [position.lat(),position.lng()]
        };
      //  console.log(newMarkerData);

     /*   let markerData = {
            lat: position.lat(),
            lng: position.lng(),
            marker: marker
        };*/
        let infowindow = new google.maps.InfoWindow({
            content: title
          });
        marker.addListener('click', function() {
            infowindow.open(this.map, marker);
          });

       
        self.markers.push(marker);

        self.newmarkers.push(newMarkerData);
        self.map.panTo(position); 

        this.markerCluster= new MarkerClusterer(this.map, this.markers,
            {imagePath: 'assets/images/m/m'});
        }
    });

     });

   
  }
 
    loadMarkers(){
        console.log('start');
        let center = this.map.getCenter(),
            bounds = this.map.getBounds(),
            zoom = this.map.getZoom();
     
        // Convert to readable format
        let centerNorm = {
            lat: center.lat(),
            lng: center.lng()
        };
        console.log('city center');
        console.log(centerNorm);
     
        let boundsNorm = {
            northEast: {
                lat: bounds.getNorthEast().lat(),
                lng: bounds.getNorthEast().lng()
            },
            southWest: {
                lat: bounds.getSouthWest().lat(),
                lng: bounds.getSouthWest().lng()
            }
        };
     
        let boundingRadius = this.getBoundingRadius(centerNorm, boundsNorm);
     
        let options = {
            lat: centerNorm.lat,
            lng: centerNorm.lng,
            maxDistance: boundingRadius
        }
        console.log('options');
     
        console.log(options);
     
        this.getMarkers(options);
     
    }
 
    getMarkers(options){
        let auth = localStorage.getItem('auth');
        let headers  = new Headers({ 'Content-Type': 'application/json' });
        headers.append('Authorization', `${auth}`);
 
       
     
        this.http.post('http://old.fotosmart.pro:3012/api/markers', JSON.stringify(options), {headers: headers})
            .map(res => res.json())
            .subscribe(marks => {
     
                console.log(marks);
               this.addMarkers(marks);
     
            });
     
    }
 
    addMarkers(markers){
        let self =this;
 
        let marker;
        let markerLatLng;
        let lat;
        let lng;
        let title;
     
        markers.forEach((marker) => {
     
            lat = marker.loc[0];
            lng = marker.loc[1];
            title = marker.name;
     
            markerLatLng = new google.maps.LatLng(lat, lng);
     
            if(!this.markerExists(lat, lng)){


             //      console.log(markerLatLng);
                marker = new google.maps.Marker({
                    title: title,
                    map: self.map,
                    animation: google.maps.Animation.DROP,
                    position: markerLatLng
                });
     
              /*let markerData = {
                    lat: lat,
                    lng: lng,
                    marker: marker
                }; */
                let infowindow = new google.maps.InfoWindow({
                    content: title
                  });
                marker.addListener('click', function() {
                    infowindow.open(this.map, marker);
                  });
     
             //  this.markers.push(markerData);
                this.markers.push(marker);
                  // end my marker 
           
               }
     
        });

           this.markerCluster= new MarkerClusterer(this.map, this.markers,
           {imagePath: 'assets/images/m/m'});

    }
 
    markerExists(lat, lng){
 
        let exists = false;
     
        this.markers.forEach((marker) => {
         if(marker.position.lat() === lat && marker.position.lng() === lng){
                exists = true;
            }
        });
     
        return exists;
     
    }
 
    getBoundingRadius(center, bounds){
        return this.getDistanceBetweenPoints(center, bounds.northEast, 'km');   
    }
 
    getDistanceBetweenPoints(pos1, pos2, units){
 
        let earthRadius = {
            miles: 3958.8,
            km: 6371
        };
 
        let R = earthRadius[units || 'miles'];
        let lat1 = pos1.lat;
        let lon1 = pos1.lng;
        let lat2 = pos2.lat;
        let lon2 = pos2.lng;
 
        let dLat = this.toRad((lat2 - lat1));
        let dLon = this.toRad((lon2 - lon1));
        let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(this.toRad(lat1)) * Math.cos(this.toRad(lat2)) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        let d = R * c;
 
        return d;
 
    }
 
    toRad(x){
        return x * Math.PI / 180;
    }
    setZoomPlus(){
        this.map.setZoom(++this.zoom);
    }
    setZoomMinus(){
        this.map.setZoom(--this.zoom);
    }

   


   getRadarRequest(type){
 
    let auth = localStorage.getItem('auth');
    let headers  = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Authorization', `${auth}`);
        let center = this.map.getCenter();
        console.log('Recieved: ' + type);
        var options = {
            location: center,
            type: type
          };
     
        this.http.post('http://old.fotosmart.pro:3012/api/nearbyplaces', JSON.stringify(options), {headers: headers})
            .map(res => res.json())
            .subscribe(res => {
              let marks = res.json.results;
              console.log(marks);
              this.createMarker(marks);
              

            //   this.addMarkers(markers);
     
            });
     
    }


    createMarker(markers){
 
        let marker;
        let markerLatLng;
        let lat;
        let lng;
        let title;
     
        markers.forEach((marker) => {
            let location = marker.geometry.location;
            lat = location.lat;
            lng = location.lng;
            title = marker.name;
            
            console.log(lat+','+lng);
            markerLatLng = new google.maps.LatLng(lat, lng);
        
     
           if(!this.markerExists(lat, lng)){
                    
                marker = new google.maps.Marker({
                    title: title,
                    map: this.map,
                    animation: google.maps.Animation.DROP,
                    position: markerLatLng,
                    icon:  {
                        url: marker.icon,
                        scaledSize: new google.maps.Size(24, 24)
                    }
                     
                });
     
          /*      let markerData = {
                    lat: lat,
                    lng: lng,
                    marker: marker
                }; */
                let infowindow = new google.maps.InfoWindow({
                    content: title
                  });
                marker.addListener('click', function() {
                    infowindow.open(this.map, marker);
                  });
     
              //  this.markers.push(markerData);
                this.markers.push(marker);
     
      }
     
        });

        this.markerCluster= new MarkerClusterer(this.map, this.markers,
            {imagePath: 'assets/images/m/m'});
     
    }
 
}